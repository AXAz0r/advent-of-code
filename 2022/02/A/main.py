WIN_COMBS = {
    'A': 'Z',
    'B': 'X',
    'C': 'Y'
}

LOSE_COMBS = {
    'A': 'Y',
    'B': 'Z',
    'C': 'X'
}

MOVE_VALUES = {
    'X': 1,
    'Y': 2,
    'Z': 3
}

# Win 6, Draw 3, Lose 0


def content() -> str:
    cnt = ''
    with open('2022/02/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def main():
    cnt = content()
    rounds = [line.strip() for line in cnt.split('\n') if line.strip()]
    points = 0
    for round in rounds:
        move, response = round.split(' ')
        response_value = MOVE_VALUES[response]
        i_win = LOSE_COMBS[move] == response
        if i_win:
            result_value = 6
        else:
            i_lose = WIN_COMBS[move] == response
            if i_lose:
                result_value = 0
            else:
                result_value = 3
        points += result_value + response_value
    print(points)


if __name__ == '__main__':
    main()
