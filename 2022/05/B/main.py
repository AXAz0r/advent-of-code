storage = {}


def get_content() -> str:
    cnt = ''
    with open('2022/05/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def get_stack(num: int) -> list:
    return storage.get(num, [])


def set_stack(num: int, stack: list):
    storage.update({num: stack})


def move_stack(amount: int, source: int, destination: int):
    s_stack = get_stack(source)
    d_stack = get_stack(destination)
    items = s_stack[-amount:]
    d_stack += items
    s_stack = s_stack[:-amount]
    set_stack(source, s_stack)
    set_stack(destination, d_stack)


def init_stacks(inv: str):
    coordinates = inv.split('\n')[-1]
    lines = []
    for line in inv.split('\n')[:-1]:
        line = line.strip()
        if line:
            lines.append(line)
    lines.reverse()
    for line in lines:
        ix = 0
        for cx, coordinate in enumerate(coordinates):
            if coordinate.strip():
                item = line[cx] if len(line) > cx else None
                if item and item.strip():
                    print(f'{coordinate}: {item}')
                    stack = get_stack(ix)
                    stack.append(item)
                    set_stack(ix, stack)
                ix += 1


def draw_stacks():
    keys = list(storage.keys())
    keys.sort()
    letters = []
    for key in keys:
        stack = get_stack(key)
        letters.append(stack[-1])
        print(f'{key}: {" ".join(stack)}')
    print(''.join(letters))


def get_move(move: str) -> tuple:
    move = move.split(' ')
    amount = int(move[1])
    source = int(move[3]) - 1
    destination = int(move[5]) - 1
    return amount, source, destination


def main():
    points = 0
    cnt = get_content()
    inventory, moves = cnt.split('\n\n')
    init_stacks(inventory)
    for move in moves.split('\n'):
        move = move.strip()
        if move:
            amount, source, destination = get_move(move)
            move_stack(amount, source, destination)
    draw_stacks()
    print(points)


if __name__ == '__main__':
    main()
