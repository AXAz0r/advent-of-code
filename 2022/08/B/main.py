import copy


def get_content() -> str:
    cnt = ''
    with open('2022/08/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def view_disance_top(tree, trees):
    count = 0
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocx == tcx and ocy < tcy:
            count += 1
            if ohg >= thg:
                break
    return count


def view_disance_bottom(tree, trees):
    count = 0
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocx == tcx and ocy > tcy:
            count += 1
            if ohg >= thg:
                break
    return count


def view_disance_left(tree, trees):
    count = 0
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocy == tcy and ocx < tcx:
            count += 1
            if ohg >= thg:
                break
    return count


def view_disance_right(tree, trees):
    count = 0
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocy == tcy and ocx > tcx:
            count += 1
            if ohg >= thg:
                break
    return count


def view_distance(tree, trees, revtrees):
    checkers = [
        [view_disance_top, 1],
        [view_disance_bottom, 0],
        [view_disance_left, 1],
        [view_disance_right, 0],
    ]
    distances = []
    for checker, direction in checkers:
        distances.append(checker(tree, revtrees if direction else trees))
    return distances


def scenic_score(distances: list[int]) -> int:
    score = 1
    for distance in distances:
        score *= distance
    return score


def main():
    cnt = get_content()
    trees = []
    rows = [ln.strip() for ln in cnt.splitlines()]
    for rix, row in enumerate(rows):
        for cix, tree in enumerate(row):
            height = int(tree)
            coord_x = cix
            coord_y = rix
            trees.append((coord_x, coord_y, height))
    revtrees = copy.deepcopy(trees)
    revtrees.reverse()
    scores = []
    for tree in trees:
        distances = view_distance(tree, trees, revtrees)
        score = scenic_score(distances)
        scores.append(score)
    print(max(scores))


if __name__ == '__main__':
    main()
