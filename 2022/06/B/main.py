BUFFER_SIZE = 14


def get_content() -> str:
    cnt = ''
    with open('2022/06/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def unique_buffer(buffer) -> bool:
    vals = []
    for bi in buffer:
        if bi in vals:
            return False
        vals.append(bi)
    return True


def main():
    cnt = get_content()
    cnt = cnt.strip()
    found = False
    index = 0
    while not found:
        buffer = []
        for ix in range(BUFFER_SIZE):
            buffer.append(cnt[index + ix])
        print(buffer)
        if unique_buffer(buffer):
            found = True
        if not found:
            index += 1
    points = index + BUFFER_SIZE
    print(points)


if __name__ == '__main__':
    main()
