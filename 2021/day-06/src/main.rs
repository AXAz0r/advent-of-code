use std::collections::HashMap;

fn first(fish: &Vec<u8>) {
    let mut gen = 0;
    let mut fish = fish.clone();
    while gen < 80 {
        let mut new = Vec::new();
        for lf in &fish {
            if lf == &0_u8 {
                new.push(6);
                new.push(8);
            } else {
                new.push(lf - 1);
            }
        }
        fish = new;
        gen += 1;
    }
    println!("First: {}", fish.len());
}

fn second(fish: &Vec<u8>) {
    let mut total = 0;
    let mut gen = 0;
    let fish = fish.clone();
    let mut ages = HashMap::<u8, u64>::new();
    for lf in fish {
        let count = if let Some(count) = ages.get(&lf) {
            count + 1
        } else {
            1
        };
        ages.insert(lf, count);
    }
    let keys: [u8; 9] = [8, 6, 0, 7, 5, 4, 3, 2, 1];
    while gen < 256 {
        let mut new = HashMap::<u8, u64>::new();
        for key in &keys {
            let key = key.clone();
            let val = if let Some(val) = ages.get(&key) {
                val.clone()
            } else {
                0
            };
            if key == 0 {
                new.insert(6, val.clone());
                new.insert(8, val.clone());
            } else {
                let tar = key - 1;
                let curr = if let Some(curr) = new.get(&tar) {
                    curr.clone()
                } else {
                    0
                };
                new.insert(tar, curr + val);
            }
        }
        ages = new;
        gen += 1;
    }
    for (_, val) in ages {
        total += val;
    }
    println!("Second: {}", total);
}

fn main() {
    let body = include_str!("input.txt");
    let fish = body.split(",").map(|num| num.trim().parse::<u8>().unwrap()).collect::<Vec<u8>>();
    first(&fish);
    second(&fish);
    println!("Hello, world!");
}
