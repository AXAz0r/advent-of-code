import json

MAX_SIZE = 100_000
DIR_MAP = []


def get_content() -> str:
    cnt = ''
    with open('2022/07/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def unique_buffer(buffer) -> bool:
    vals = []
    for bi in buffer:
        if bi in vals:
            return False
        vals.append(bi)
    return True


def is_command(line: str) -> bool:
    return line.startswith('$')


def command_name(line: str) -> str:
    return line.split(' ')[1]


def command_arg(line: str) -> str:
    return line.split(' ')[-1]


def is_dir(line: str) -> bool:
    return line.startswith('dir')


def get_size(line: str) -> int:
    return int(line.split(' ')[0])


def get_name(line: str) -> str:
    return line.split(' ')[-1]


def walk_path(lines: list[str]) -> dict:
    path = ''
    by_path = {}
    for line in lines:
        if is_command(line):
            cmd = command_name(line)
            if cmd == 'cd':
                arg = command_arg(line)
                if arg == '..':
                    path = '/'.join(path.split('/')[:-1])
                else:
                    path += f'/{arg}'
            # elif cmd == 'ls':
            #     buffer = []
        else:
            if not is_dir(line):
                file_path = f'{path}/{get_name(line)}'
                while '//' in file_path:
                    file_path = file_path.replace('//', '/')
                by_path.update({file_path: get_size(line)})
    return by_path


def nest_dict(data: dict) -> dict:
    nested = {}
    for path, size in data.items():
        path = path.strip('/')
        parts = path.split('/')
        if len(parts) == 1:
            nested.update({parts[0]: size})
        else:
            current = nested
            for part in parts[:-1]:
                if part not in current:
                    current.update({part: {}})
                current = current[part]
            current.update({parts[-1]: size})
    return nested


def nested_total_size(data: dict) -> int:
    total = 0
    for key, val in data.items():
        if isinstance(val, dict):
            nested_size = nested_total_size(val)
            DIR_MAP.append(nested_size)
            addition = nested_size
        else:
            addition = val
        total += addition
    return total


def get_points() -> int:
    points = 0
    for item in DIR_MAP:
        if item <= MAX_SIZE:
            points += item
    return points


def main():
    cnt = get_content()
    lines = [ln.strip() for ln in cnt.split('\n') if ln.strip()]
    data = walk_path(lines)
    nested = nest_dict(data)
    _ = nested_total_size(nested)
    print(get_points())


if __name__ == '__main__':
    main()
