import math

LOG_LINES = []


def log(msg: str):
    print(msg)
    LOG_LINES.append(msg)


def save_log():
    with open('log.txt', 'w', encoding='utf-8') as f:
        f.write('\n'.join(LOG_LINES))


def get_content() -> str:
    with open('../../../2022/09/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


class Position(object):
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def distance(self, other) -> int:
        return int(math.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2))

    def move(self, x: int, y: int):
        log(f'{self.name} moving to {self.x + x}, {self.y + y}')
        self.x += x
        self.y += y

    def is_distant(self, other) -> bool:
        x_diff = abs(self.x - other.x)
        y_diff = abs(self.y - other.y)
        return x_diff > 1 or y_diff > 1

    def move_to(self, other):
        while self.is_distant(other):
            x_diff = abs(self.x - other.x)
            y_diff = abs(self.y - other.y)
            if x_diff and y_diff:
                self.move(1 if self.x < other.x else -1, 1 if self.y < other.y else -1)
            else:
                if x_diff > y_diff:
                    self.move(1 if self.x < other.x else -1, 0)
                else:
                    self.move(0, 1 if self.y < other.y else -1)

    @staticmethod
    def test():
        me = Position('T', 0, 0)
        other = Position('H', 3, 1)
        me.move_to(other)


def get_moves(cnt: str) -> list[tuple[str, int]]:
    moves = []
    for line in cnt.splitlines():
        line = line.strip()
        if line:
            direction, distance = line[0], int(line[1:])
            moves.append((direction, distance))
    return moves


def draw(positions: list[str], stdout: bool = False):
    min_x = min_y = 0
    max_x = max_y = 0
    for pos in positions:
        x, y = pos[1:-1].split(':')
        x, y = int(x), int(y)
        if x < min_x:
            min_x = x
        if x > max_x:
            max_x = x
        if y < min_y:
            min_y = y
        if y > max_y:
            max_y = y
    max_x += 1
    rows = []
    for y in range(max_y, min_y - 1, -1):
        row = []
        for x in range(min_x, max_x + 1):
            if x == 0 and y == 0:
                sign = 'S'
            elif f'({x}:{y})' in positions:
                sign = '#'
            else:
                sign = '.'
            row.append(sign)
        rows.append(row)
    if stdout:
        for row in rows:
            print(''.join(row))
    else:
        with open('output.txt', 'w', encoding='utf-8') as f:
            for row in rows:
                f.write(''.join(row) + '\n')


def main():
    positions = []
    cnt = get_content()
    moves = get_moves(cnt)
    head = Position('H', 0, 0)
    tail = Position('T', 0, 0)
    mix = 0
    for direction, distance in moves:
        mix += 1
        log(f'Move {mix}: {direction} {distance}')
        for _ in range(distance):
            if direction == 'U':
                head.move(0, 1)
            elif direction == 'D':
                head.move(0, -1)
            elif direction == 'R':
                head.move(1, 0)
            elif direction == 'L':
                head.move(-1, 0)
            tail.move_to(head)
            pl = f'({tail.x}:{tail.y})'
            if pl not in positions:
                positions.append(pl)
        log('')
    log(f'Positions: {len(positions)}')
    draw(positions, stdout=False)
    save_log()


if __name__ == '__main__':
    main()
