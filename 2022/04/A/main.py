def get_content() -> str:
    cnt = ''
    with open('2022/04/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def get_range(rg: str):
    start = int(rg.split('-')[0])
    end = int(rg.split('-')[1])
    return range(start, end + 1)


def main():
    points = 0
    cnt = get_content()
    pairs = [line.strip() for line in cnt.split('\n') if line.strip()]
    for pair in pairs:
        range_a, range_b = pair.split(',')
        range_a = get_range(range_a)
        range_b = get_range(range_b)
        containment_a = all([rai in range_b for rai in range_a])
        containment_b = all([rbi in range_a for rbi in range_b])
        if containment_a or containment_b:
            points += 1
    print(points)


if __name__ == '__main__':
    main()
