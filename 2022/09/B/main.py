import math


def get_content() -> str:
    with open('../../../2022/09/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


class Position(object):
    def __init__(self, name, x, y):
        self.name = name
        self.x = x
        self.y = y

    def distance(self, other) -> int:
        return int(math.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2))

    def move(self, x: int, y: int):
        self.x += x
        self.y += y

    def is_distant(self, other) -> bool:
        x_diff = abs(self.x - other.x)
        y_diff = abs(self.y - other.y)
        return x_diff > 1 or y_diff > 1

    def move_to(self, other):
        while self.is_distant(other):
            x_diff = abs(self.x - other.x)
            y_diff = abs(self.y - other.y)
            if x_diff and y_diff:
                self.move(1 if self.x < other.x else -1, 1 if self.y < other.y else -1)
            else:
                if x_diff > y_diff:
                    self.move(1 if self.x < other.x else -1, 0)
                else:
                    self.move(0, 1 if self.y < other.y else -1)


def get_moves(cnt: str) -> list[tuple[str, int]]:
    moves = []
    for line in cnt.splitlines():
        line = line.strip()
        if line:
            direction, distance = line[0], int(line[1:])
            moves.append((direction, distance))
    return moves


def main():
    positions = []
    cnt = get_content()
    moves = get_moves(cnt)
    pieces = []
    for n in range(10):
        pieces.append(Position(f'{n}', 0, 0))
    for direction, distance in moves:
        head = pieces[0]
        for _ in range(distance):
            if direction == 'U':
                head.move(0, 1)
            elif direction == 'D':
                head.move(0, -1)
            elif direction == 'R':
                head.move(1, 0)
            elif direction == 'L':
                head.move(-1, 0)
            prev_piece = head
            for piece in pieces[1:]:
                piece.move_to(prev_piece)
                prev_piece = piece
            tail = pieces[-1]
            pl = f'{tail.x}:{tail.y}'
            if pl not in positions:
                positions.append(pl)
    print(f'Positions: {len(positions)}')


if __name__ == '__main__':
    main()
