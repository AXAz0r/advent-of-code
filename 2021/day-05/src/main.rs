use std::collections::HashMap;
use std::ops::Range;

#[derive(Clone)]
pub struct Vent {
    pub x: i64,
    pub y: i64,
    pub v: u64,
}

impl Vent {
    pub fn new(x: i64, y: i64) -> Self {
        Self { x, y, v: 0 }
    }

    pub fn trigger(&mut self) {
        self.v += 1;
    }

    pub fn check(&self) -> bool {
        self.v >= 2
    }
}

#[derive(Clone, Debug)]
pub struct Instruction {
    pub xa: i64,
    pub ya: i64,
    pub xb: i64,
    pub yb: i64,
}

impl Instruction {
    pub fn new(xa: i64, ya: i64, xb: i64, yb: i64) -> Self {
        Self { xa, ya, xb, yb }
    }
}

#[derive(Clone)]
pub struct Floor {
    pub vents: HashMap<(i64, i64), Vent>,
    pub instructions: Vec<Instruction>,
}

impl Floor {
    pub fn parse() -> Self {
        let body = include_str!("input.txt");
        let lines = body.split("\n").map(|line| line.trim().to_string()).collect::<Vec<String>>();
        let vents = HashMap::new();
        let mut instructions = Vec::new();
        for line in &lines {
            let line = line.clone();
            let sides = line.split("->").map(|side| side.trim().to_string()).collect::<Vec<String>>();
            let left = sides[0].clone();
            let left_coords = left.split(",").map(|lc| lc.trim().parse::<i64>().unwrap()).collect::<Vec<i64>>();
            let right = sides[1].clone();
            let right_coords = right.split(",").map(|rc| rc.trim().parse::<i64>().unwrap()).collect::<Vec<i64>>();
            let instruction = Instruction::new(left_coords[0], left_coords[1], right_coords[0], right_coords[1]);
            instructions.push(instruction);
        }
        println!("Parsed {} instructions.", instructions.len());
        Self { vents, instructions }
    }

    pub fn first(&mut self) {
        for instruction in &self.instructions {
            if instruction.xa == instruction.xb {
                let range = range(instruction.ya, instruction.yb);
                for y in range {
                    if let Some(vent) = self.vents.get_mut(&(instruction.xa, y)) {
                        vent.trigger();
                    } else {
                        let mut vent = Vent::new(instruction.xa, y);
                        vent.trigger();
                        self.vents.insert((instruction.xa, y), vent);
                    }
                }
            } else if instruction.ya == instruction.yb {
                let range = range(instruction.xa, instruction.xb);
                for x in range {
                    if let Some(vent) = self.vents.get_mut(&(x, instruction.ya)) {
                        vent.trigger();
                    } else {
                        let mut vent = Vent::new(x, instruction.ya);
                        vent.trigger();
                        self.vents.insert((x, instruction.ya), vent);
                    }
                }
            }
        }
        let mut total = 0;
        for (_, vent) in &self.vents {
            if vent.check() {
                total += 1;
            }
        }
        println!("First: {}", total);
    }

    pub fn second(&mut self) {
        for instruction in &self.instructions {
            if instruction.xa == instruction.xb {
                let range = range(instruction.ya, instruction.yb);
                for y in range {
                    if let Some(vent) = self.vents.get_mut(&(instruction.xa, y)) {
                        vent.trigger();
                    } else {
                        let mut vent = Vent::new(instruction.xa, y);
                        vent.trigger();
                        self.vents.insert((instruction.xa, y), vent);
                    }
                }
            } else if instruction.ya == instruction.yb {
                let range = range(instruction.xa, instruction.xb);
                for x in range {
                    if let Some(vent) = self.vents.get_mut(&(x, instruction.ya)) {
                        vent.trigger();
                    } else {
                        let mut vent = Vent::new(x, instruction.ya);
                        vent.trigger();
                        self.vents.insert((x, instruction.ya), vent);
                    }
                }
            } else if is_diagonal(instruction.xa, instruction.ya, instruction.xb, instruction.yb) {
                let mut xcur = instruction.xa;
                let mut ycur = instruction.ya;
                let xmod = if instruction.xa > instruction.xb { -1 } else { 1 };
                let ymod = if instruction.ya > instruction.yb { -1 } else { 1 };
                let xbor = if xmod == 1 {
                    if instruction.xa > instruction.xb {
                        instruction.xa
                    } else {
                        instruction.xb
                    }
                } else {
                    if instruction.xa > instruction.xb {
                        instruction.xb
                    } else {
                        instruction.xa
                    }
                };
                let ybor = if ymod == 1 {
                    if instruction.ya > instruction.yb {
                        instruction.ya
                    } else {
                        instruction.yb
                    }
                } else {
                    if instruction.ya > instruction.yb {
                        instruction.yb
                    } else {
                        instruction.ya
                    }
                };
                while xcur != (xbor + xmod) && ycur != (ybor + ymod) {
                    if let Some(vent) = self.vents.get_mut(&(xcur, ycur)) {
                        vent.trigger();
                    } else {
                        let mut vent = Vent::new(xcur, ycur);
                        vent.trigger();
                        self.vents.insert((xcur, ycur), vent);
                    }
                    xcur += xmod;
                    ycur += ymod;
                }
            }
        }
        let mut total = 0;
        for (_, vent) in &self.vents {
            if vent.check() {
                total += 1;
            }
        }
        println!("Second: {}", total);
    }
}

fn is_diagonal(xa: i64, ya: i64, xb: i64, yb: i64) -> bool {
    let xd = if xa > xb { xa - xb } else { xb - xa };
    let yd = if ya > yb { ya - yb } else { yb - ya };
    yd == xd
}

fn range(a: i64, b: i64) -> Range<i64> {
    if a > b {
        b..a + 1
    } else {
        a..b + 1
    }
}

fn first(floor: &Floor) {
    let mut floor = floor.clone();
    floor.first();
}

fn second(floor: &Floor) {
    let mut floor = floor.clone();
    floor.second();
}

fn main() {
    let floor = Floor::parse();
    first(&floor);
    second(&floor);
}
