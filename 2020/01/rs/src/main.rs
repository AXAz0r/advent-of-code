const INPUT_PATH: &str = "../input.txt";

fn first(numbers: &Vec<i64>) {
    'outer: for fno in numbers {
        for sno in numbers {
            if sno + fno == 2020 {
                println!("First Result: {}", sno * fno);
                break 'outer;
            }
        }
    }
}

fn second(numbers: &Vec<i64>) {
    'outer: for fno in numbers {
        for sno in numbers {
            for tno in numbers {
                if sno + fno + tno == 2020 {
                    println!("Second Result: {}", sno * fno * tno);
                    break 'outer;
                }
            }
        }
    }
}

fn main() {
    let content = std::fs::read_to_string(INPUT_PATH).unwrap();
    let numbers = content.split("\n").filter_map(|line| {
        if !line.is_empty() {
            Some(line.parse::<i64>().unwrap())
        } else {
            None
        }
    }).collect::<Vec<i64>>();
    first(&numbers);
    second(&numbers);
}
