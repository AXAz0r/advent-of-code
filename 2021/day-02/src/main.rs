#[derive(Default)]
pub struct Location {
    pub horizontal: i64,
    pub vertical: i64,
}

#[derive(Default)]
pub struct Aim {
    pub horizontal: i64,
    pub vertical: i64,
    pub aim: i64
}

pub struct Instruction {
    pub horizontal: i64,
    pub vertical: i64,
}

impl Instruction {
    pub fn new(x: i64, y: i64) -> Self {
        Self { horizontal: x, vertical: y }
    }
}

fn first(insts: &Vec<Instruction>) {
    let mut loc = Location::default();
    for ins in insts {
        loc.horizontal += ins.horizontal;
        loc.vertical += ins.vertical;
    }
    println!("First: {}", loc.horizontal * loc.vertical);
}

fn second(insts: &Vec<Instruction>) {
    let mut aim = Aim::default();
    for ins in insts {
        if ins.horizontal == 0 {
            aim.aim += ins.vertical;
        } else {
            aim.horizontal += ins.horizontal;
            aim.vertical += ins.horizontal * aim.aim;
        }
    }
    println!("Second: {}", aim.horizontal * aim.vertical);
}

fn main() {
    let body = include_str!("input.txt");
    let instructions = body.split("\n").map(|line| {
        let line = line.trim();
        let pieces = line.split(" ").collect::<Vec<&str>>();
        let direction = pieces[0];
        let amount = pieces[1].parse::<i64>().unwrap();
        match direction {
            "forward" => {
                Instruction::new(amount, 0)
            }
            "down" => {
                Instruction::new(0, amount)
            }
            "up" => {
                Instruction::new(0, -amount)
            }
            _ => panic!("Unknown direction!")
        }
    }).collect::<Vec<Instruction>>();
    first(&instructions);
    second(&instructions);
}
