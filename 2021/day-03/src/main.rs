fn first(lines: &Vec<String>) {
    let mut index = 0;
    let border = lines.get(0).unwrap().len();
    let mut gamma = Vec::new();
    let mut epsillon = Vec::new();
    while index < border {
        let mut zeroes = 0;
        let mut ones = 0;
        for line in lines {
            let digit = line.chars().map(|chr| chr.to_string()).collect::<Vec<String>>()[index].parse::<usize>().unwrap();
            if digit == 0 {
                zeroes += 1;
            } else {
                ones += 1;
            }
        }
        if zeroes > ones {
            gamma.push(0.to_string());
            epsillon.push(1.to_string());
        } else {
            gamma.push(1.to_string());
            epsillon.push(0.to_string());
        }
        index += 1;
    }
    let gstr = gamma.join("");
    let estr = epsillon.join("");
    let gval = i64::from_str_radix(&gstr, 2).unwrap();
    let eval = i64::from_str_radix(&estr, 2).unwrap();
    println!("First - Gamma: {} ({}) | Epsillon: {} ({}) | Total: {}", &gstr, gval, &estr, eval, gval * eval);
}

fn oxygen(lines: &Vec<String>, border: usize) -> i64 {
    let mut lines = lines.clone();
    while lines.len() > 1 {
        let mut index = 0;
        while index < border {
            let mut zeroes = 0;
            let mut ones = 0;
            for line in &lines {
                let digit = line.chars().map(|chr| chr.to_string()).collect::<Vec<String>>()[index].parse::<usize>().unwrap();
                if digit == 0 {
                    zeroes += 1;
                } else {
                    ones += 1;
                }
            }
            let mut valid = Vec::new();
            for line in &lines {
                let line = line.clone();
                let digit = line.chars().map(|chr| chr.to_string()).collect::<Vec<String>>()[index].parse::<usize>().unwrap();
                if zeroes > ones {
                    if digit == 0 {
                        valid.push(line);
                    }
                } else if ones >= zeroes {
                    if digit == 1 {
                        valid.push(line);
                    }
                }
            }
            lines = valid;
            index += 1;
        }
    }
    let value = lines.get(0).unwrap();
    i64::from_str_radix(value, 2).unwrap()
}

fn scrubber(lines: &Vec<String>, border: usize) -> i64 {
    let mut lines = lines.clone();
    while lines.len() > 1 {
        let mut index = 0;
        while index < border {
            let mut zeroes = 0;
            let mut ones = 0;
            for line in &lines {
                let digit = line.chars().map(|chr| chr.to_string()).collect::<Vec<String>>()[index].parse::<usize>().unwrap();
                if digit == 0 {
                    zeroes += 1;
                } else {
                    ones += 1;
                }
            }
            let mut valid = Vec::new();
            for line in &lines {
                let line = line.clone();
                let digit = line.chars().map(|chr| chr.to_string()).collect::<Vec<String>>()[index].parse::<usize>().unwrap();
                if zeroes <= ones {
                    if digit == 0 {
                        valid.push(line);
                    }
                } else if ones < zeroes {
                    if digit == 1 {
                        valid.push(line);
                    }
                }
            }
            lines = valid;
            index += 1;
            if lines.len() == 1 {
                break;
            }
        }
    }
    let value = lines.get(0).unwrap();
    i64::from_str_radix(value, 2).unwrap()
}

fn second(lines: &Vec<String>) {
    let border = lines.get(0).unwrap().len();
    let oxygen = oxygen(lines, border);
    let scrubber = scrubber(lines, border);
    println!("Second - Oxygen: {} | Scrubber: {} | Total: {}", oxygen, scrubber, oxygen * scrubber);
}

fn main() {
    let body = include_str!("input.txt");
    let lines = body.split("\n").map(|line| { line.trim().to_string() }).collect::<Vec<String>>();
    first(&lines);
    second(&lines);
}
