def get_content() -> str:
    cnt = ''
    with open('2022/08/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def is_visible_top(tree, trees):
    visible = True
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocx == tcx and ocy < tcy:
            if ohg >= thg:
                visible = False
                break
    return visible


def is_visible_bottom(tree, trees):
    visible = True
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocx == tcx and ocy > tcy:
            if ohg >= thg:
                visible = False
                break
    return visible


def is_visible_left(tree, trees):
    visible = True
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocy == tcy and ocx < tcx:
            if ohg >= thg:
                visible = False
                break
    return visible


def is_visible_right(tree, trees):
    visible = True
    tcx, tcy, thg = tree
    for other in trees:
        ocx, ocy, ohg = other
        if ocy == tcy and ocx > tcx:
            if ohg >= thg:
                visible = False
                break
    return visible


def is_visible(tree, trees):
    visible = False
    checkers = [
        is_visible_top,
        is_visible_bottom,
        is_visible_left,
        is_visible_right,
    ]
    for checker in checkers:
        if checker(tree, trees):
            visible = True
            break
    return visible


def main():
    cnt = get_content()
    trees = []
    rows = [ln.strip() for ln in cnt.splitlines()]
    for rix, row in enumerate(rows):
        for cix, tree in enumerate(row):
            height = int(tree)
            coord_x = cix
            coord_y = rix
            trees.append((coord_x, coord_y, height))
    points = 0
    for tree in trees:
        visible = is_visible(tree, trees)
        if visible:
            points += 1
    print(points)


if __name__ == '__main__':
    main()
