#[derive(Clone, Debug)]
pub struct Cell {
    pub x: u8,
    pub y: u8,
    pub v: u8,
    pub a: bool,
}

impl Cell {
    pub fn new(x: u8, y: u8, v: u8) -> Self {
        Self { x, y, v, a: false }
    }

    pub fn activate(&mut self) {
        self.a = true;
    }
}

#[derive(Clone, Debug)]
pub struct Board {
    pub id: u8,
    pub rows: Vec<Vec<Cell>>,
    pub won: bool,
}

impl Board {
    pub fn from_rows(id: u8, rows: &Vec<String>) -> Self {
        let rows = rows.clone();
        let mut board_rows = Vec::new();
        for (iy, row) in rows.iter().enumerate() {
            let row = row.clone().trim().to_string();
            let pieces = row.split(" ").collect::<Vec<&str>>();
            let mut board_row = Vec::new();
            for (ix, piece) in pieces.iter().enumerate() {
                if !piece.is_empty() {
                    let val = piece.parse::<u8>().unwrap();
                    let cell = Cell::new(ix as u8, iy as u8, val);
                    board_row.push(cell);
                }
            }
            if !board_row.is_empty() {
                board_rows.push(board_row);
            }
        }
        let board = Self { id, rows: board_rows, won: false };
        board
    }

    pub fn valid(&self) -> bool {
        let mut valid = true;
        if self.rows.len() == 5 {
            for row in &self.rows {
                if row.len() != 5 {
                    valid = false;
                    break;
                }
            }
        } else {
            valid = false;
        }
        if !valid {
            println!("Board {} is not valid.", self.id);
        }
        valid
    }

    pub fn activate(&mut self, num: u8) {
        for row in &mut self.rows {
            for cell in row {
                if cell.v == num {
                    cell.activate();
                }
            }
        }
    }

    pub fn unsum(&self) -> u64 {
        let mut total = 0;
        for row in &self.rows {
            for cell in row {
                if !cell.a {
                    total += cell.v as u64;
                }
            }
        }
        total
    }

    pub fn check(&mut self) -> bool {
        let mut active = false;
        for row in &self.rows {
            let mut active_row = true;
            for cell in row {
                if !cell.a {
                    active_row = false;
                    break;
                }
            }
            if active_row {
                active = true;
                break;
            }
        }
        if !active {
            for ix in 0..self.rows.len() {
                let mut active_col = true;
                let mut cells = Vec::new();
                for row in &self.rows {
                    let cell = row.get(ix).unwrap();
                    cells.push(cell);
                }
                for cell in cells {
                    if !cell.a {
                        active_col = false;
                        break;
                    }
                }
                if active_col {
                    active = true;
                    break;
                }
            }
        }
        active
    }
}

#[derive(Clone, Debug)]
pub struct Game {
    pub numbers: Vec<u8>,
    pub boards: Vec<Board>,
}

impl Game {
    pub fn new(numbers: Vec<u8>, boards: Vec<Board>) -> Self {
        Self { numbers, boards }
    }

    pub fn valid(&self) -> bool {
        let mut valid = true;
        for board in &self.boards {
            if !board.valid() {
                valid = false;
                break;
            }
        }
        valid
    }

    pub fn parse() -> Game {
        let body = include_str!("input.txt");
        let lines = body.split("\n").map(|ln| { ln.to_string() }).collect::<Vec<String>>();
        let mut boards = Vec::new();
        let mut rows = Vec::new();
        let mut numbers = Vec::new();
        let mut board_id = 0;
        for (ix, line) in lines.iter().enumerate() {
            let line = line.clone().trim().to_string();
            if ix == 0 {
                numbers = line.trim().split(",").map(|num| num.parse::<u8>().unwrap()).collect::<Vec<u8>>();
            } else {
                if line.is_empty() && !rows.is_empty() {
                    let board = Board::from_rows(board_id, &rows);
                    boards.push(board);
                    rows.clear();
                    board_id += 1;
                } else {
                    rows.push(line);
                }
            }
        }
        Game::new(numbers, boards)
    }

    pub fn activate(&mut self, num: u8) -> bool {
        let mut done = false;
        for board in &mut self.boards {
            board.activate(num);
            if board.check() {
                board.won = true;
                let unsum = board.unsum();
                println!("First: Board {} has won with {} and unsum {} for a total {}.", &board.id, num, unsum, unsum * num as u64);
                done = true;
                break;
            }
        }
        done
    }

    pub fn first(&mut self) {
        for num in self.numbers.clone() {
            if self.activate(num.clone()) {
                break;
            }
        }
    }

    pub fn second(&mut self) {
        let mut last: Option<u8> = None;
        for num in self.numbers.clone() {
            let mut dead = Vec::new();
            for board in &mut self.boards {
                board.activate(num);
                board.won = board.check();
                if !board.won {
                    dead.push(board.clone());
                }
            }
            if dead.len() == 1 {
                let db = dead.get(0).unwrap();
                last = Some(db.id);
            }
            if dead.is_empty() {
                let lid = last.unwrap();
                for board in &self.boards {
                    if board.id == lid {
                        let unsum = board.unsum();
                        println!("Second: Board {} has won with {} and unsum {} for a total {}.", &board.id, num, unsum, unsum * num as u64);
                        break;
                    }
                }
                break;
            }
        }
    }
}

fn first(game: &Game) {
    let mut game = game.clone();
    game.first();
}

fn second(game: &Game) {
    let mut game = game.clone();
    game.second();
}

fn main() {
    println!("Parsing game...");
    let game = Game::parse();
    if game.valid() {
        first(&game);
        second(&game);
    } else {
        println!("The game is not valid.");
    }
    println!("Parsed {} numbers and {} boards.", game.numbers.len(), game.boards.len());
}
