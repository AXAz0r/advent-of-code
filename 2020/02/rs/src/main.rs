const INPUT_PATH: &str = "../input.txt";

fn first(content: &str) {
    let mut valid = 0;
    content.split_terminator("\n").for_each(|line| {
        let line_pieces = line.split(": ").collect::<Vec<&str>>();
        let rules = line_pieces.get(0).unwrap();
        let password = line_pieces.get(1).unwrap();
        let rule_pieces = rules.split(" ").collect::<Vec<&str>>();
        let counts = rule_pieces.get(0).unwrap();
        let character = rule_pieces.get(1).unwrap();
        let count_pieces = counts.split("-").collect::<Vec<&str>>();
        let low_count = count_pieces.get(0).unwrap().parse::<i64>().unwrap();
        let high_count = count_pieces.get(1).unwrap().parse::<i64>().unwrap();
        // TITS
        let mut count = 0;
        for char in password.chars() {
            if char.to_string() == character.to_string() {
                count += 1;
            }
        }
        if high_count >= count && low_count <= count {
            valid += 1;
        }
    });
    println!("First Valid: {}", valid);
}

fn second(content: &str) {
    let mut valid = 0;
    content.split_terminator("\n").for_each(|line| {
        let line_pieces = line.split(": ").collect::<Vec<&str>>();
        let rules = line_pieces.get(0).unwrap();
        let password = line_pieces.get(1).unwrap();
        let rule_pieces = rules.split(" ").collect::<Vec<&str>>();
        let counts = rule_pieces.get(0).unwrap();
        let character = rule_pieces.get(1).unwrap();
        let count_pieces = counts.split("-").collect::<Vec<&str>>();
        let low_count = count_pieces.get(0).unwrap().parse::<usize>().unwrap() - 1;
        let high_count = count_pieces.get(1).unwrap().parse::<usize>().unwrap() - 1;
        // ASS
        let chars = password.chars().collect::<Vec<char>>();
        let first_char = chars.get(low_count).unwrap();
        let second_char = chars.get(high_count).unwrap();
        if first_char.to_string() == character.to_string() || second_char.to_string() == character.to_string() {
            if !(first_char.to_string() == character.to_string() && second_char.to_string() == character.to_string()) {
                valid += 1;
            }
        }
    });
    println!("Second Valid: {}", valid);
}

fn main() {
    let content = std::fs::read_to_string(INPUT_PATH).unwrap();
    first(&content);
    second(&content);
}
