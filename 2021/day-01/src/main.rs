fn first(numbers: &Vec<i64>) {
    let mut prev = None;
    let mut count = 0;
    for number in numbers {
        if let Some(pd) = prev {
            if number > pd {
                count += 1;
            }
        }
        prev = Some(number);
    }
    println!("First: {}", count);
}

fn second(numbers: &Vec<i64>) {
    let mut prev = None;
    let mut count = 0;
    let mut index = 0;
    while index < numbers.len() {
        let mut total = 0;
        let indexes = [index, index + 1, index + 2];
        for ix in indexes {
            if let Some(number) = numbers.get(ix) {
                total += number;
            }
        }
        index += 1;
        if let Some(pd) = prev {
            dbg!([pd, total]);
            if total > pd {
                count += 1;
            }
        }
        prev = Some(total);
    }
    println!("Second: {}", count);
}

fn main() {
    let input = include_str!("input.txt");
    let numbers = input.split("\n").map(|line| {line.trim().parse::<i64>().unwrap()}).collect::<Vec<i64>>();
    first(&numbers);
    second(&numbers);
}
