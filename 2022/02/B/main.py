TRANSLATION = {
    'A': 'R',
    'B': 'P',
    'C': 'S'
}

RES_VALUES = {
    'X': 0,
    'Y': 3,
    'Z': 6
}

MOVE_VALUES = {
    'R': 1,
    'P': 2,
    'S': 3
}

# Win 6, Draw 3, Lose 0

RESPONSES = {
    'X': {
        'A': 'S',
        'B': 'R',
        'C': 'P'
    },
    'Y': {
        'A': 'R',
        'B': 'P',
        'C': 'S'
    },
    'Z': {
        'A': 'P',
        'B': 'S',
        'C': 'R'
    }
}


def content() -> str:
    cnt = ''
    with open('2022/02/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def main():
    cnt = content()
    rounds = [line.strip() for line in cnt.split('\n') if line.strip()]
    points = 0
    for round in rounds:
        move, result = round.split(' ')
        result_value = RES_VALUES[result]
        response = RESPONSES[result][move]
        response_value = MOVE_VALUES[response]
        points += result_value + response_value
    print(points)


if __name__ == '__main__':
    main()
