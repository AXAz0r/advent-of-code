import string


def content() -> str:
    cnt = ''
    with open('2022/03/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def get_groups(sacks: list) -> list:
    groups = []
    group = []
    for sack in sacks:
        group.append(sack)
        if len(group) == 3:
            groups.append(group)
            group = []
    return groups


def get_common_item(group: list) -> str:
    common = None
    for item in group[0]:
        if item in group[1] and item in group[2]:
            common = item
            break
    return common


def get_value(char: str) -> int:
    val = string.ascii_letters.index(char) + 1
    print(f'{char} = {val}')
    return val


def main():
    points = 0
    cnt = content()
    sacks = [line.strip() for line in cnt.split('\n') if line.strip()]
    groups = get_groups(sacks)
    for group in groups:
        common = get_common_item(group)
        if common:
            points += get_value(common)
    print(points)


if __name__ == '__main__':
    main()
