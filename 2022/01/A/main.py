def get_content() -> str:
    cnt = ''
    with open('2022/01/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def main():
    cnt = get_content()
    chunks = []
    for chunk in cnt.split('\n\n'):
        chunk = chunk.strip()
        if chunk:
            chunks.append(chunk)
    values = []
    for chunk in chunks:
        value = 0
        items = chunk.split('\n')
        for item in items:
            item = item.strip()
            if item:
                value += int(item)
        values.append(value)
    highest = max(values)
    print(highest)


if __name__ == '__main__':
    main()
