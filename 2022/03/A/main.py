import string


def content() -> str:
    cnt = ''
    with open('2022/03/input.txt', 'r', encoding='utf-8') as f:
        cnt = f.read()
    return cnt


def compartments(sack: str) -> list:
    comp_a = sack[:len(sack) // 2]
    comp_b = sack[len(sack) // 2:]
    return [comp_a, comp_b]


def common_items(comp_a: str, comp_b: str) -> list:
    common = []
    for item in comp_a:
        if item in comp_b:
            if item not in common:
                common.append(item)
    return common


def value(char: str) -> int:
    val = string.ascii_letters.index(char) + 1
    print(f'{char} = {val}')
    return val


def sack_value(common: list) -> int:
    pts = 0
    for item in common:
        pts += value(item)
    return pts


def main():
    cnt = content()
    points = 0
    sacks = [line.strip() for line in cnt.split('\n') if line.strip()]
    for sack in sacks:
        comp_a, comp_b = compartments(sack)
        common = common_items(comp_a, comp_b)
        if common:
            points += sack_value(common)
    print(points)


if __name__ == '__main__':
    main()
